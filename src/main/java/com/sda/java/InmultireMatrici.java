package com.sda.java;

public class InmultireMatrici {

    public void inmultireMatrici(int[][] unu, int[][] doi, int linie1, int col1, int linie2, int col2) {
        int sum = 0;
        int[][] produs = new int[linie1][col2];

        for (int i = 0; i < linie1; i++) {
            for (int j = 0; j < col2; j++) {
                for (int k = 0; k < linie2; k++) {
                    sum = sum + unu[i][k] * doi[k][j];
                }
                produs[i][j] = sum;
                sum = 0;

                System.out.print((produs[i][j] + "\t"));
            }
            System.out.println("\n");
        }
    }
}
