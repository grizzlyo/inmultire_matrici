package com.sda.java;

import java.util.Scanner;

public class ApplicationRunner {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Introduceti numarul de coloane a primei matrici: ");
        int col1 = s.nextInt();
        System.out.println("Introduceti numarul de linii a primei matrici: ");
        int linie1 = s.nextInt();
        int[][] unu = new int[linie1][col1];


        System.out.println("Introduceti elementele primei matrici: ");
        for (int i = 0; i < linie1; i++) {
            for (int j = 0; j < col1; j++) {
                unu[i][j] = s.nextInt();
            }
        }

        System.out.println("Introduceti numarul de coloane a matricei doi: ");
        int col2 = s.nextInt();
        System.out.println("Introduceti numarul de linii a matricei doi: ");
        int linie2 = s.nextInt();
        int [][] doi = new int[linie2][col2];

        if (col1 != linie2) {
            System.out.println("Matricile nu se pot inmulti!");
        }
        else {
            System.out.println("Introduceti elementele matricei doi: ");
            for (int i = 0; i < linie2; i++) {
                for (int j = 0; j < col2; j++) {
                    doi[i][j] = s.nextInt();
                }
            }
            InmultireMatrici inmultireMatrici = new InmultireMatrici();
            System.out.println("Afisez produsul matricilor: ");

            inmultireMatrici.inmultireMatrici(unu, doi, linie1, col1, linie2, col2);
        }



    }
}
